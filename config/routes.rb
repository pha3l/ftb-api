Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'application#redirect_to_modpacks_index'

  resources :modpacks, only: [:index, :show], defaults: {format: :json}, :id => /.*/ do
    resources :versions, only: [:index], defaults: {format: :json} do
      collection do
        get 'latest'
      end
    end
  end
end
