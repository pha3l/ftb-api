json.extract! modpack, :name, :update_time, :page_link
json.versions URI.encode(modpack_versions_url(modpack.name))