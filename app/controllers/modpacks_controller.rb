class ModpacksController < ApplicationController
  before_action :set_modpack, only: [:show, :edit, :update, :destroy]

  # GET /modpacks
  # GET /modpacks.json
  def index
    @modpacks = FtbModpack.get_all
  end

  # GET /modpacks/1
  # GET /modpacks/1.json
  def show
    @modpack
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_modpack
      @modpack = FtbModpack.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def modpack_params
      params.fetch(:modpack, {})
    end
end
