class VersionsController < ApplicationController
  before_action :set_versions

  # GET /versions
  # GET /versions.json
  def index
    @versions
  end

  # GET /versions/1
  # GET /versions/1.json
  def latest
    @version = @versions.sort_by { |v| v.release_time }.last
    render :show
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_versions
      @versions = Version.get_all(FtbModpack.find(URI.decode(params[:modpack_id])))
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def version_params
      params.fetch(:version, {}).permit(:modpack_id)
    end
end
