class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def redirect_to_modpacks_index
    redirect_to '/modpacks'
  end
end
