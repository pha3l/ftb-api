class FtbModpack

  def initialize(attrs)
    @name = attrs[:name]
    @update_time = attrs[:update_time]
    @page_link = attrs[:pagelink]
  end

  attr_accessor :name
  attr_accessor :update_time
  attr_accessor :page_link

  class << self
    def get_all
      first = Nokogiri::HTML(open(Settings.ftb.base_url + Settings.ftb.modpacks_path))
          .css('ul.listing.listing-project')
          .css('li').map do |mp|
        modpack_name = mp.css('div.info.name')
                           .css('div.name-wrapper.overflow-tip')
                           .css('a')
        modpack_update = mp.css('div.info.stats').css('p.e-update-date').css('abbr')[0].attributes['data-epoch'].value
        FtbModpack.new(name: modpack_name[0].text,
                       update_time: Time.at(modpack_update.to_i).to_datetime,
                       pagelink: Settings.ftb.base_url + modpack_name[0].attributes['href'].value)
      end
    end

    def find(name)
      first = Nokogiri::HTML(open(Settings.ftb.base_url + Settings.ftb.modpacks_path))
                  .css('ul.listing.listing-project')
                  .css('li').css('div.info.name')
                  .css('div.name-wrapper.overflow-tip')
                  .css('a').find { |n| n.text == name }

      unless first.nil?

        modpack_update = first.parent.parent.parent.css('div.info.stats').css('p.e-update-date').css('abbr')[0].attributes['data-epoch'].value
        FtbModpack.new(name: first.text,
                       update_time: Time.at(modpack_update.to_i).to_datetime,
                       pagelink: Settings.ftb.base_url + first.attributes['href'].value)
      else
        raise "Couldn't identity modpack from given string: #{name}"
      end

    end
  end
end