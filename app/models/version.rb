class Version

  def initialize(attrs)
    @version = attrs[:version]
    @download_link = attrs[:download_link]
    @release_time = attrs[:release_time]
  end

  attr_accessor :version
  attr_accessor :download_link
  attr_accessor :release_time

  class << self

    def get_all(modpack)
      agent = Mechanize.new
      files_page = agent.get(modpack.page_link + '/files')

      agent.follow_redirect = false   # Set this now so that we don't try to download the servers

      files_page.css(".listing").css("tr.project-file-list-item").css(".project-file-name-container > a.overflow-tip").map do |v|
        dllink = v.attributes['href']
        download_url = Settings.ftb.base_url + dllink + '#additional-files'
        download_page = agent.get(download_url)

        version = download_page.css('td.project-file-name').css('div.project-file-name-container').css('a')[0].text.partition('_').last.partition('.zip').first
        release_time = download_page.css('td.project-file-date-uploaded').css('abbr')[0].attributes['data-epoch'].value

        redirect_to_file_page = download_page.links.find do |l|
          l.attributes['class'] == 'button tip fa-icon-download icon-only'
        end.click

        download_link = redirect_to_file_page.response['location']
        Version.new(version: version, download_link: download_link, release_time: Time.at(release_time.to_i).to_datetime)
      end


    end
  end
end